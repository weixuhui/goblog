package main

import (
	"database/sql"
	"goblog/app/http/middlewares"
	"goblog/bootstrap"
	"goblog/config"
	c "goblog/pkg/config"
	"goblog/pkg/database"
	"goblog/pkg/logger"
	"net/http"
)

var db *sql.DB

func init() {
	config.Initialize()
}

func main() {
	database.Initialize()
	db = database.DB

	bootstrap.SetupDB()
	router := bootstrap.SetupRoute()

	err := http.ListenAndServe(":"+c.GetString("app.port"), middlewares.RemoveTrailingSlash(router))
	logger.LogError(err)
}
