package logger

import "log"

func CheckError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func LogError(err error)  {
	if err != nil {
		log.Println(err)
	}
}