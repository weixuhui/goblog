package bootstrap

import (
	"goblog/pkg/route"
	routers "goblog/routes"

	"github.com/gorilla/mux"
)

func SetupRoute() *mux.Router {
	router := mux.NewRouter()
	routers.RegisterWebRoutes(router)

	route.SetRouter(router)

	return router
}
