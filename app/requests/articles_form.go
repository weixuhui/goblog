package requests

import (
	"goblog/app/models/article"

	"github.com/thedevsaddam/govalidator"
)

func ValidateArticleForm(data article.Article) map[string][]string {
	rules := govalidator.MapData{
		"title": []string{"required", "min:3", "max:40"},
		"body":  []string{"required", "min:10"},
	}

	message := govalidator.MapData{
		"title": []string{
			"required:标题必填",
			"min:标题不得小于3个字符",
			"max:标题不得大于40个字符",
		},
		"body": []string{
			"required:内容必填",
			"min:内容不得小于10个字符",
		},
	}

	opts := govalidator.Options{
		Data:          &data,
		Rules:         rules,
		Messages:      message,
		TagIdentifier: "valid",
	}

	return govalidator.New(opts).ValidateStruct()

}
